#
# Copyright 2015, Broadcom Corporation
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of Broadcom Corporation.
#

NAME := RESTful_Smart_Server_Application

$(NAME)_SOURCES    := restful_smart_server_app.c \
                      wiced_bt_cfg.c \
                      ble_hello_sensor.c

$(NAME)_INCLUDES   := .

$(NAME)_COMPONENTS += daemons/bt_internet_gateway \
                      daemons/bt_internet_gateway/restful_smart_server \
                      libraries/drivers/bluetooth \
                      utilities/command_console

VALID_PLATFORMS    := BCM9WCDPLUS114 \
                      BCM943341WCD1 \
                      BCM943909WCD* \
                      BCM9WCD1AUDIO \
                      BCM943438WLPTH_2 \
                      BCM94343WWCDA_ext \
                      BCM94343W_AVN

WIFI_CONFIG_DCT_H  := wifi_config_dct.h
